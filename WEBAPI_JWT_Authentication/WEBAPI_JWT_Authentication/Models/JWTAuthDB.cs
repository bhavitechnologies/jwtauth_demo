﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore;

//using System;
namespace WEBAPI_JWT_Authentication.Models
{
    public class JWTAuthDB : DbContext
    {
        public JWTAuthDB() : base("JWTAuthDBContext")
        { }

        public DbSet<Login> logins {get;set;}
        public DbSet<Employee> employees { get; set; }  
    }
}