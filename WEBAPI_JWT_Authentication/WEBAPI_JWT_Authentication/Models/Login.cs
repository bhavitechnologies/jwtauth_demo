﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAPI_JWT_Authentication.Models
{
    public class Login
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}