﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WEBAPI_JWT_Authentication.Models;

namespace WEBAPI_JWT_Authentication.Controllers
{

    [Authorize]
    public class ValuesController : ApiController
    {
        private JWTAuthDB db = new JWTAuthDB();

        // GET: api/values
        public IQueryable<Employee> Get()
        {
            return db.employees;
        }
        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }


        // POST: api/values
        public IHttpActionResult Post([FromBody]Employee value)
        {
            db.employees.Add(value);
            return Json(db.SaveChanges());
        }

        // PUT api/values/5
        public IHttpActionResult Put(int id, [FromBody]Employee value)
        {
            db.Entry(value).State = EntityState.Modified;
            return Json(db.SaveChanges());
        }

        //PUT api/values/5
        public IHttpActionResult Delete(int id)
        {
            db.employees.Remove(db.employees.FirstOrDefault(x => x.Id == id));
            return Json(db.SaveChanges());
        }
    }
}
